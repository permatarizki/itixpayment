# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from rest_framework import viewsets
from rest_framework import generics

from . import serializers
from . import models

# Create your views here.
class ParkingViewSet(viewsets.ModelViewSet):
    """Handles CRUD for model Client"""

    serializer_class = serializers.ParkingSerializer
    queryset = models.Parking.objects.all()

    

class ParkingByCarNumber(generics.ListCreateAPIView):

    queryset = models.Parking.objects.all()
    serializer_class = serializers.ParkingByCarNumberSerializer

    def get_queryset(self):
        parking = self.kwargs['platenumber']
        return models.Parking.objects.filter(visitor_car_plate__iexact=parking)
