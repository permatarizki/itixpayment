from django.conf.urls import url
from django.conf.urls import include

from rest_framework.routers import DefaultRouter

from . import views


router = DefaultRouter()
router.register('parking', views.ParkingViewSet)


urlpatterns = [
    url(r'', include(router.urls)),
    url(r'^parkingbycarnumber/(?P<platenumber>.+)$',views.ParkingByCarNumber.as_view(), name='auditresult-filterlist'),
]
