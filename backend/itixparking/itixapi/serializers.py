from rest_framework import serializers
from . import models


class ParkingSerializer(serializers.ModelSerializer):
    """A serializer for Parking"""

    class Meta:
        model = models.Parking
        fields = '__all__'

class ParkingByCarNumberSerializer(serializers.ModelSerializer):
    #tour = TourDepartureTravelerSerializer(many=False, read_only=True)
    #auditresult = ParkingSerializer(many=False, read_only=True)

    class Meta:
        model = models.Parking
        fields = '__all__'
