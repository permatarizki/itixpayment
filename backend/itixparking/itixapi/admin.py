# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from . import models

# Register your models here.
admin.site.site_header = "I-TIX Administrator"

admin.site.register(models.Parking)
