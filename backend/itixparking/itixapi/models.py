# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Parking(models.Model):
    """ Parking"""

    created_on = models.DateTimeField(auto_now_add=True)
    entrance_time = models.DateTimeField(auto_now_add=True)
    exit_time = models.DateTimeField(blank=True, null=True)
    visitor_name = models.CharField(max_length=100)
    visitor_car_plate = models.CharField(max_length=20)

    def __str__(self):
        """Return the model as a string."""

        return '{} - {}'.format(self.entrance_time, self.visitor_car_plate)
