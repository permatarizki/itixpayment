/**
 * Created by mata on 11/30/18.
 */

import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Modal,
    Alert,
    FlatList,
    Dimensions,
    ActivityIndicator
} from "react-native";
import { connect } from "react-redux";
import QRCodeScanner from 'react-native-qrcode-scanner';

import MaterialIcons from "react-native-vector-icons/MaterialIcons";

class History extends Component {
    static navigatorStyle = {
        navBarTitleTextCentered:true,
        navBarTextColor: 'white',
        navBarBackgroundColor: '#572B2B',
    };

    constructor(props) {
        super(props);
        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent);
    }

    state = {
        parkingcar:null,
        refreshing: false,

    }

    componentWillMount() {
        let url = 'https://i-tixparking.com/api/parking/';
        fetch(url, {
            method: 'GET',
        })
            .catch(err => {
                console.log(err);
                this.setState({errorMessage: err})
            })
            .then(res => res.json())
            .then(parsedRes => {
                console.log(parsedRes)
                this.setState({parkingcar: parsedRes})
            });
    }

    handleRefresh = () => {
        this.setState({refreshing: true});
        let url = 'https://i-tixparking.com/api/parking/';
        fetch(url, {
            method: 'GET',
        })
            .catch(err => {
                console.log(err);
                this.setState({errorMessage: err})
            })
            .then(res => res.json())
            .then(parsedRes => {
                console.log(parsedRes)
                this.setState({parkingcar: parsedRes, refreshing:false})
            });
    }

    renderDateTime(datetime){
        let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        datetime = new Date(datetime)
        let time_now_date = datetime.getDate();
        let time_now_month = datetime.getMonth();
        let time_now_year = datetime.getFullYear();
        let time_now_hours = datetime.getHours();
        let time_now_minutes = datetime.getMinutes();

        if(time_now_minutes < 10){
            time_now_minutes = '0'+time_now_minutes.toString()
        }
        if(time_now_hours < 10){
            time_now_hours = '0'+time_now_hours.toString()
        }
        if(time_now_date < 10){
            time_now_date = '0'+time_now_date.toString()
        }

        return(
            <Text>{time_now_hours}:{time_now_minutes} ({time_now_date}-{months[time_now_month]}-{time_now_year.toString()})</Text>
        )
    }

    render() {
        let refreshing = null
        if(this.state.refreshing){
            refreshing = (
                <ActivityIndicator size="small" color="#dedede" />
            )
        }
        return (
            <View style={styles.mainMenu}>
                {refreshing}
                <FlatList
                    contentContainerStyle={{width:'100%'}}
                    data={this.state.parkingcar}
                    refreshing={this.state.refreshing}
                    onRefresh={this.handleRefresh}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={(info) =>
                    {
                        console.log('info', info)
                        return(
                            <TouchableOpacity disabled={true} onPress={() => {}}>
                                <View style={styles.listItem}>
                                    <View style={{padding:1}}>
                                        <View style={{margin:5, padding:10, flexWrap: "wrap" }}>
                                            <View style={{flexDirection: 'row'}}>
                                                <Text style={{color: '#572B2B', width: 100, fontWeight: 'bold'}}>Nama</Text>
                                                <Text> : </Text>
                                                <Text>{info.item.visitor_name}</Text>
                                            </View>
                                            <View style={{flexDirection: 'row'}}>
                                                <Text style={{color: '#572B2B', width: 100, fontWeight: 'bold'}}>Plat Nomor</Text>
                                                <Text> : </Text>
                                                <Text>{info.item.visitor_car_plate}</Text>
                                            </View>
                                            <View style={{flexDirection: 'row'}}>
                                                <Text style={{color: '#572B2B', width: 100, fontWeight: 'bold'}}>Jam Masuk</Text>
                                                <Text> : </Text>
                                                <Text>{this.renderDateTime(info.item.entrance_time)}</Text>
                                            </View>
                                            <View style={{flexDirection: 'row'}}>
                                                <Text style={{color: '#572B2B', width: 100, fontWeight: 'bold'}}>Jam Keluar</Text>
                                                <Text> : </Text>
                                                <Text>{(info.item.exit_time == null)?"Not Available":this.renderDateTime(info.item.exit_time)}</Text>
                                            </View>
                                        </View>

                                    </View>
                                </View>
                            </TouchableOpacity>
                        )
                    }}
                />
            </View >
        )
    }
}

const styles = StyleSheet.create({
    mainMenu: {
        flex: 1,
        justifyContent:'center',
        alignItems: 'center'
    },
    listItem: {
        width: (Dimensions.get('window').width)-10,
        margin: 5,
        paddingVertical:10,
        backgroundColor: "#e5e5e5",
        flexDirection: "column",
        borderRadius: 5,
        borderWidth:0.5,
        borderColor:'#ADAEAD',
        shadowColor: '#ADAEAD',
        shadowOpacity: 0.8,
        shadowRadius: 5,
        elevation: 3,
    },
})

export default connect(null, null)(History);