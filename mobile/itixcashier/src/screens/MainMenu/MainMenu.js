/**
 * Created by mata on 11/30/18.
 */

import React, {Component} from "react";
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Modal,
    Alert,
    Button,
    AsyncStorage
} from "react-native";
import {connect} from "react-redux";
import QRCodeScanner from 'react-native-qrcode-scanner';

import MaterialIcons from "react-native-vector-icons/MaterialIcons";

class MainMenu extends Component {
    static navigatorStyle = {
        navBarTitleTextCentered:true,
        navBarTextColor: 'white',
        navBarBackgroundColor: '#572b2b',
    };

    constructor(props) {
        super(props);
        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent);
    }

    state = {
        modalQRCamera: false,
        customerdata: null,
        platenumber: '',
        lastparking: null,
        modalDataCustomer: false
    }

    onScan = () => {
        this.setState({modalQRCamera: true})
    }

    onPayment = () => {

        let url = 'https://i-tixparking.com/api/parking/';


        var data = new FormData();
        var timenow = new Date()
        data.append('entrance_time', this.state.lastparking.entrance_time)
        data.append('exit_time', timenow.toISOString())
        // data.append('id', this.state.lastparking.id)
        data.append('visitor_car_plate', this.state.lastparking.visitor_car_plate)
        data.append('visitor_name', this.state.lastparking.visitor_name)

        console.log('data PUT', data)

        fetch(
            url+this.state.lastparking.id+'/',
            {
                method: "PUT",
                headers: {
                    'Accept': 'application/json',
                },
                body: data
            }
        )
            .then(res => {
                    console.log('res PUT ', res)
                    Alert.alert(
                        'Pembayaran BERHASIL! ',
                        'Data Payment already sent to server \n\n ',
                        [
                            {text: 'Cancel', onPress: () => this.setState({modalDataCustomer: false}), style: 'cancel'},
                            {text: 'OK', onPress: () => this.setState({modalDataCustomer: false})},
                        ],
                        {cancelable: false}
                    )

                }
            )
            .catch(err => {
                console.log('ERROR:', err)
                Alert.alert("ERROR!")
            });

    }

    onSuccess = (e) => {
        //TODO filter Data
        let info = e.data
        let infos = info.split(";");
        let url = 'https://i-tixparking.com/api/parkingbycarnumber/';

        if (infos.length != 2) {
            Alert.alert('ERROR', 'QR Code tidak terdeteksi\nPastikan Anda men-scan QR Code dari aplikasi I-TIX')
        } else {
            //TODO send data to server
            fetch(url + infos[1], {
                method: 'GET',
            })
                .catch(err => {
                    console.log(err);
                    Alert.alert('ERROR accessing server')
                })
                .then(res => res.json())
                .then(parsedRes => {
                    console.log('get data', parsedRes)
                    this.setState({customerdata: parsedRes, lastparking: parsedRes[parsedRes.length - 1]})
                })

        }
        console.log('info', infos[1])
        this.setState({modalQRCamera: false, modalDataCustomer: true, platenumber: infos[1]})
    }

    render() {
        let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        let customer = null
        if (this.state.customerdata != null) {
            console.log('customer', this.state.customerdata)
            if(this.state.customerdata.length == 0){
                customer = (
                    <View style={{justifyContent:'center',  alignItems:'center'}}>
                        <View style={{padding:50, borderWidth:1, borderColor:'#dedede'}}>
                            <Text>No Parking Data for <Text style={{fontWeight: 'bold'}}>{this.state.platenumber}</Text></Text>
                        </View>
                        <View style={{paddingTop:50, width: 100}}>
                            <Button
                                onPress={() => {this.setState({modalDataCustomer: false})}}
                                color="#043673"
                                title="OK"
                            />
                        </View>
                    </View>
                )
            }else{
                let lastparking = this.state.customerdata[this.state.customerdata.length - 1]
                if (lastparking.exit_time == null) {
                    //TODO hitung selisih waktu
                    let time_now = new Date()
                    let entrance_time = new Date(lastparking.entrance_time)

                    let parking_duration = Math.ceil(Math.abs(entrance_time.getTime() - time_now.getTime()) / 3600000);
                    let tagihan = parking_duration * 3000;

                    let time_now_date = time_now.getDate();
                    let time_now_month = time_now.getMonth();
                    let time_now_year = time_now.getFullYear();
                    let time_now_hours = time_now.getHours();
                    let time_now_minutes = time_now.getMinutes();

                    let entrance_time_date = entrance_time.getDate();
                    let entrance_time_month = entrance_time.getMonth();
                    let entrance_time_year = entrance_time.getFullYear();
                    let entrance_time_hours = entrance_time.getHours();
                    let entrance_time_minutes = entrance_time.getMinutes();

                    if(time_now_minutes < 10){
                        time_now_minutes = '0'+time_now_minutes.toString()
                    }
                    if(time_now_hours < 10){
                        time_now_hours = '0'+time_now_hours.toString()
                    }
                    if(time_now_date < 10){
                        time_now_date = '0'+time_now_date.toString()
                    }

                    if(entrance_time_minutes < 10){
                        entrance_time_minutes = '0'+entrance_time_minutes.toString()
                    }
                    if(entrance_time_hours < 10){
                        entrance_time_hours = '0'+entrance_time_hours.toString()
                    }
                    if(entrance_time_date < 10){
                        entrance_time_date = '0'+entrance_time_date.toString()
                    }

                    // let tagihan =
                    customer = (
                        <View style={{paddingLeft:20,borderWidth:1, borderColor:'#dedede',paddingVertical: 30}}>
                            <View style={{flexDirection: 'row'}}>
                                <Text style={{color: '#572B2B', width: 100, fontWeight: 'bold'}}>Nama</Text>
                                <Text> : </Text>
                                <Text>{lastparking.visitor_name}</Text>
                            </View>
                            <View style={{flexDirection: 'row'}}>
                                <Text style={{color: '#572B2B', width: 100, fontWeight: 'bold'}}>Plat Nomor</Text>
                                <Text> : </Text>
                                <Text>{lastparking.visitor_car_plate}</Text>
                            </View>
                            <View style={{flexDirection: 'row'}}>
                                <Text style={{color: '#572B2B', width: 100, fontWeight: 'bold'}}>Jam Masuk</Text>
                                <Text> : </Text>
                                <Text>{entrance_time_hours}:{entrance_time_minutes}
                                    ({entrance_time_date}-{months[entrance_time_month]}-{entrance_time_year})</Text>
                            </View>
                            <View style={{flexDirection: 'row'}}>
                                <Text style={{color: '#572B2B', width: 100, fontWeight: 'bold'}}>Jam Keluar</Text>
                                <Text> : </Text>
                                <Text>{time_now_hours}:{time_now_minutes}
                                    ({time_now_date}-{months[time_now_month]}-{time_now_year})</Text>
                            </View>
                            <View style={{paddingTop: 20, flexDirection: 'row'}}>
                                <Text style={{color: '#572B2B', width: 100, fontWeight: 'bold'}}>Parking Duration</Text>
                                <Text> : </Text>
                                <Text>{parking_duration} hours</Text>
                            </View>
                            <View style={{flexDirection: 'row'}}>
                                <Text style={{color: '#572B2B', width: 100, fontWeight: 'bold'}}>Tagihan</Text>
                                <Text> : </Text>
                                <Text style={{color: 'red'}}>Rp. {tagihan}</Text>
                            </View>
                            <View style={{paddingTop: 30, width: '80%', borderRadius: 5}}>
                                <View style={{width: '100%', flexDirection: 'row', justifyContent: 'space-around'}}>
                                    <View style={{width: 100}}>
                                        <Button
                                            onPress={() => {
                                                this.setState({modalDataCustomer: false})
                                            }}
                                            color="#043673"
                                            title="CANCEL"
                                        />
                                    </View>

                                    <View style={{width: 100}}>
                                        <Button
                                            onPress={this.onPayment}
                                            color="#043673"
                                            title="BAYAR"
                                        />
                                    </View>
                                </View>
                            </View>
                        </View>
                    )
                } else {
                    customer = (
                        <View style={{justifyContent:'center',  alignItems:'center'}}>
                            <View style={{padding:50, borderWidth:1, borderColor:'#dedede'}}>
                                <Text>No bills for <Text style={{fontWeight: 'bold'}}>{this.state.platenumber}</Text></Text>
                            </View>
                            <View style={{paddingTop:50, width: 100}}>
                                <Button
                                    onPress={() => {this.setState({modalDataCustomer: false})}}
                                    color="#043673"
                                    title="OK"
                                />
                            </View>
                        </View>
                    )
                }
            }
        } else {
            customer = (
                <View style={{justifyContent:'center',  alignItems:'center'}}>
                    <View style={{padding:50, borderWidth:1, borderColor:'#dedede'}}>
                        <Text>No Parking Data for <Text style={{fontWeight: 'bold'}}>{this.state.platenumber}</Text></Text>
                    </View>
                    <View style={{paddingTop:50, width: 100}}>
                        <Button
                            onPress={() => {this.setState({modalDataCustomer: false})}}
                            color="#043673"
                            title="OK"
                        />
                    </View>
                </View>
            )
        }

        return (
            <View style={styles.mainMenu}>
                <View style={{paddingBottom: 50}}>
                </View>
                <View style={{borderWidth: 0, borderColor: 'white', alignItems: 'center'}}>
                    <TouchableOpacity onPress={this.onScan} style={styles.roundIcon} activeOpacity={0.9}>
                        <MaterialIcons
                            name="photo-camera"
                            size={100}
                            color="#572B2B"
                        />
                        <Text style={{color: '#572B2B'}}>Scan for Payment</Text>
                    </TouchableOpacity>
                </View>
                <Modal
                    animationType="none"
                    transparent={false}
                    visible={this.state.modalQRCamera}
                    onRequestClose={() => {
                        this.setState({modalQRCamera: false})
                    }}>
                    <View style={{justifyContent: 'center', alignItems: 'center'}}>
                        <View>
                            <QRCodeScanner
                                onRead={this.onSuccess}
                                // topContent={
                                //     <Text style={styles.centerText}>
                                //         Arahkan camera pada <Text style={styles.textBold}>QR_code</Text>
                                //     </Text>
                                // }
                                // bottomContent={
                                //     <TouchableOpacity style={styles.buttonTouchable}>
                                //         <Text style={styles.buttonText}>Selesai</Text>
                                //     </TouchableOpacity>
                                // }
                                showMarker={true}
                                cameraStyle={{height: "100%"}}
                            />
                        </View>
                    </View>
                </Modal>
                <Modal
                    animationType="none"
                    transparent={false}
                    visible={this.state.modalDataCustomer}
                    onRequestClose={() => {
                        this.setState({modalDataCustomer: false})
                    }}>
                    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                        <View style={{paddingBottom: 50}}>
                            <Text style={{fontSize: 20, color: '#572B2B'}}>I-TIX PARKING BILL</Text>
                        </View>
                        <View style={{padding: 10}}>
                            {customer}
                        </View>
                    </View>
                </Modal>

            </View >
        )
    }
}

const styles = StyleSheet.create({
    mainMenu: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    roundIcon: {
        borderWidth: 4,
        borderColor: '#dbb29d',
        alignItems: 'center',
        justifyContent: 'center',
        width: 200,
        height: 200,
        backgroundColor: '#fff',
        borderRadius: 200,
    },
})

export default connect(null, null)(MainMenu);