import { Navigation } from "react-native-navigation";
import { Provider } from "react-redux";
import React, { Component } from 'react';
import {Platform} from "react-native"

import MainMenu from "./src/screens/MainMenu/MainMenu";
import History from "./src/screens/History/History";
import configureStore from "./src/store/configureStore";
import Icon from 'react-native-vector-icons/Ionicons';


const store = configureStore();

// Register Screens
Navigation.registerComponent(
    "itixcashier.MainMenu",
    () => MainMenu,
    store,
    Provider
);
Navigation.registerComponent(
    "itixcashier.History",
    () => History,
    store,
    Provider
);

// Navigation.startSingleScreenApp({
//     screen: {
//         screen: "itixcashier.MainMenu",
//         title: "I-TIX PARKING",
//         label: "I-TIX PARKING"
//     },
// });

Promise.all([
    Icon.getImageSource("ios-cash", 30),
    Icon.getImageSource("logo-buffer", 30)
]).then(sources => {
    Navigation.startTabBasedApp({
        tabs: [
            {
                label: 'Payment',
                screen: 'itixcashier.MainMenu', // this is a registered name for a screen
                icon: sources[0],
                //selectedIcon: require('../img/one_selected.png'), // iOS only
                title: 'I-TIX Cashier Payment',

            },
            {
                label: 'History',
                screen: 'itixcashier.History',
                icon: sources[1],
                //selectedIcon: require('../img/two_selected.png'), // iOS only
                title: 'I-TIX Parking History',
            }
        ]
    });
})