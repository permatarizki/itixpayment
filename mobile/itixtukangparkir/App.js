import { Navigation } from "react-native-navigation";
import { Provider } from "react-redux";
import React, { Component } from 'react';

import MainMenu from "./src/screens/MainMenu/MainMenu";
import configureStore from "./src/store/configureStore";
const store = configureStore();

// Register Screens
Navigation.registerComponent(
    "itix.MainMenu",
    () => MainMenu,
    store,
    Provider
);


Navigation.startSingleScreenApp({
    screen: {
        screen: "itix.MainMenu",
        title: "I-TIX Parking Gateway"
    },
});