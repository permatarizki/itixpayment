/**
 * Created by mata on 11/29/18.
 */


import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Modal,
    Alert
} from "react-native";
import { connect } from "react-redux";
import QRCodeScanner from 'react-native-qrcode-scanner';

import MaterialIcons from "react-native-vector-icons/MaterialIcons";

class MainMenu extends Component {
    static navigatorStyle = {
        navBarTitleTextCentered:true,
        navBarTextColor: 'white',
        navBarBackgroundColor: '#572b2b',
    };

    constructor(props) {
        super(props);
        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent);
    }

    state = {
        modalQRCamera: false,
    }

    onScan = () => {
        this.setState({ modalQRCamera: true })
    }

    onSuccess = (e) => {
        //TODO filter Data
        let info = e.data
        let infos = info.split(";");
        let url = 'https://i-tixparking.com/api/parking/';

        if(infos.length != 2){
            Alert.alert('ERROR', 'QR Code tidak terdeteksi\nPastikan Anda men-scan QR Code dari aplikasi I-TIX')
        }else{
            //TODO send data to server

            var data = new FormData();
            data.append('visitor_name', infos[0])
            data.append('visitor_car_plate',  infos[1])
            console.log('data:',data)

            fetch(
                url,
                {
                    method: "POST",
                    headers: {
                        'Accept': 'application/json',
                    },
                    body:data
                }
            )
                .then(res =>{
                    console.log('response', res)

                    Alert.alert(
                        'Scan BERHASIL! ',
                        'Selamat Datang '+infos[0]+' di kampus Prasmul!!! \n\n ',
                        [
                            { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                            { text: 'OK', onPress: () => console.log('OK Pressed') },
                        ],
                        { cancelable: false }
                    )

                    }
                )
                .catch(err => {
                    console.log('ERROR:',err)
                    Alert.alert("ERROR!")
                });

        }

        this.setState({modalQRCamera: false })
    }

    render() {
        return (
            <View style={styles.mainMenu}>
                <View style={{ borderWidth: 0, borderColor: 'white', alignItems: 'center' }}>
                    <TouchableOpacity onPress={this.onScan} style={styles.roundIcon} activeOpacity={0.9}>
                        <MaterialIcons
                            name="photo-camera"
                            size={80}
                            color="#572b2b"
                        />
                        <Text style={{ color: '#572b2b' }}>Click for QR Code Scan</Text>
                    </TouchableOpacity>
                </View>
                <Modal
                    animationType="fade"
                    transparent={false}
                    visible={this.state.modalQRCamera}
                    onRequestClose={() => { this.setState({ modalQRCamera: false }) }}>
                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <View>
                            <QRCodeScanner
                                onRead={this.onSuccess}
                                // topContent={
                                //     <Text style={styles.centerText}>
                                //         Arahkan camera pada <Text style={styles.textBold}>QR_code</Text>
                                //     </Text>
                                // }
                                // bottomContent={
                                //     <TouchableOpacity style={styles.buttonTouchable}>
                                //         <Text style={styles.buttonText}>Selesai</Text>
                                //     </TouchableOpacity>
                                // }
                                showMarker={true}
                                cameraStyle={{ height: "100%" }}
                                cameraType="front"
                            />
                        </View>
                    </View>
                </Modal>
            </View >
        )
    }
}

const styles = StyleSheet.create({
    mainMenu: {
        flex: 1,
        justifyContent:'center',
        alignItems: 'center'
    },
    roundIcon: {
        borderWidth: 4,
        borderColor: '#dbb29d',
        alignItems: 'center',
        justifyContent: 'center',
        width: 200,
        height: 200,
        backgroundColor: '#fff',
        borderRadius: 200,
    },
})

export default connect(null, null)(MainMenu);