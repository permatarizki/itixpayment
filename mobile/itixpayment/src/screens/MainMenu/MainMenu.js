/**
 * Created by mata on 11/29/18.
 */

/**
 * Created by mata on 11/29/18.
 */


import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Modal,
    Alert,
    Image,
    Button,
    AsyncStorage
} from "react-native";
import { connect } from "react-redux";

import DefaultInput from "../../components/DefaultInput/DefaultInput";
import MaterialIcons from "react-native-vector-icons/FontAwesome";
import QRCode from 'react-native-qrcode';

class MainMenu extends Component {
    static navigatorStyle = {
        navBarTextColor: 'white',
        navBarBackgroundColor: '#043673',
        navBarButtonColor: '#043673',
        // topBarElevationShadowEnabled: false,
        navBarHidden: true
    };

    constructor(props) {
        super(props);
        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent);
    }

    componentWillMount(){
        console.log('haiii')
        AsyncStorage.getItem("app:name").then((value1) => {
            console.log('get 1',value1)
            if(value1){
                this.setState({nama:value1})
            }
        })
        AsyncStorage.getItem("app:plat").then((value1) => {
            console.log('get 2', value1)
            if(value1){
                this.setState({plat:value1})
            }
        })
    }

    state = {
        modalDataCustomer:false,
        customerdata:null,
        lastparking:null,
        isQRShow:false,
        isIdentified:false,
        isSettingName:false,
        nama:'',
        plat:''
    }

    onPress = () => {
        if(this.state.nama == ''){
            Alert.alert('ERROR', 'Please set your name')
        }else{
            this.setState({isQRShow:true})
        }

    }

    onCheckParking = () => {
        if(this.state.nama == ''){
            Alert.alert('ERROR', 'Please set your name')
        }else{
            this.setState({modalDataCustomer:true})
            let url = 'https://i-tixparking.com/api/parkingbycarnumber/';

            fetch(url + this.state.plat, {
                method: 'GET',
            })
                .catch(err => {
                    console.log(err);
                    Alert.alert('ERROR accessing server')
                })
                .then(res => res.json())
                .then(parsedRes => {
                    console.log('get data', parsedRes)
                    this.setState({customerdata: parsedRes, lastparking: parsedRes[parsedRes.length - 1]})
                })
        }

    }

    onFinish = () => {
        this.setState({isQRShow:false})
    }

    onFinishSetName = () => {
        AsyncStorage.setItem("app:name", this.state.nama)
        AsyncStorage.setItem("app:plat", this.state.plat)
        this.setState({isSettingName:false})
    }

    onEdit = () => {
        this.setState({isSettingName:true})
    }

    onSetName = () => {
        this.setState({isSettingName:true})
        // this.setState({nama:'Mata', plat:'B 1234 KLW'})
    }

    render() {

        let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        let customer = null
        if (this.state.customerdata != null) {
            console.log('customer', this.state.customerdata)
            if(this.state.customerdata.length == 0){
                customer = (
                    <View style={{justifyContent:'center',  alignItems:'center'}}>
                        <View style={{padding:50, borderWidth:1, borderColor:'#dedede'}}>
                            <Text>No Parking Data for <Text style={{fontWeight: 'bold'}}>{this.state.plat}</Text></Text>
                        </View>
                        <View style={{paddingTop:50, width: 100}}>
                            <Button
                                onPress={() => {this.setState({modalDataCustomer: false})}}
                                color="#043673"
                                title="OK"
                            />
                        </View>
                    </View>
                )
            }else{
                let lastparking = this.state.customerdata[this.state.customerdata.length - 1]
                if (lastparking.exit_time == null) {
                    //TODO hitung selisih waktu
                    let time_now = new Date()
                    let entrance_time = new Date(lastparking.entrance_time)

                    let parking_duration = Math.ceil(Math.abs(entrance_time.getTime() - time_now.getTime()) / 3600000);
                    let tagihan = parking_duration * 3000;

                    let time_now_date = time_now.getDate();
                    let time_now_month = time_now.getMonth();
                    let time_now_year = time_now.getFullYear();
                    let time_now_hours = time_now.getHours();
                    let time_now_minutes = time_now.getMinutes();

                    let entrance_time_date = entrance_time.getDate();
                    let entrance_time_month = entrance_time.getMonth();
                    let entrance_time_year = entrance_time.getFullYear();
                    let entrance_time_hours = entrance_time.getHours();
                    let entrance_time_minutes = entrance_time.getMinutes();

                    if(time_now_minutes < 10){
                        time_now_minutes = '0'+time_now_minutes.toString()
                    }
                    if(time_now_hours < 10){
                        time_now_hours = '0'+time_now_hours.toString()
                    }
                    if(time_now_date < 10){
                        time_now_date = '0'+time_now_date.toString()
                    }

                    if(entrance_time_minutes < 10){
                        entrance_time_minutes = '0'+entrance_time_minutes.toString()
                    }
                    if(entrance_time_hours < 10){
                        entrance_time_hours = '0'+entrance_time_hours.toString()
                    }
                    if(entrance_time_date < 10){
                        entrance_time_date = '0'+entrance_time_date.toString()
                    }

                    // let tagihan =
                    customer = (
                        <View style={{paddingHorizontal:50, borderWidth:1, borderColor:'#dedede',paddingVertical: 30}}>
                            <View style={{flexDirection: 'row'}}>
                                <Text style={{color: '#043673', width: 100, fontWeight: 'bold'}}>Nama</Text>
                                <Text> : </Text>
                                <Text>{lastparking.visitor_name}</Text>
                            </View>
                            <View style={{flexDirection: 'row'}}>
                                <Text style={{color: '#043673', width: 100, fontWeight: 'bold'}}>Plat Nomor</Text>
                                <Text> : </Text>
                                <Text>{lastparking.visitor_car_plate}</Text>
                            </View>
                            <View style={{flexDirection: 'row'}}>
                                <Text style={{color: '#043673', width: 100, fontWeight: 'bold'}}>Jam Masuk</Text>
                                <Text> : </Text>
                                <Text>{entrance_time_hours}:{entrance_time_minutes}
                                    ({entrance_time_date}-{months[entrance_time_month]}-{entrance_time_year})</Text>
                            </View>
                            <View style={{flexDirection: 'row'}}>
                                <Text style={{color: '#043673', width: 100, fontWeight: 'bold'}}>Jam Keluar</Text>
                                <Text> : </Text>
                                <Text>{time_now_hours}:{time_now_minutes}
                                    ({time_now_date}-{months[time_now_month]}-{time_now_year})</Text>
                            </View>
                            <View style={{paddingTop: 20, flexDirection: 'row'}}>
                                <Text style={{color: '#043673', width: 100, fontWeight: 'bold'}}>Parking Duration</Text>
                                <Text> : </Text>
                                <Text>{parking_duration} hours</Text>
                            </View>
                            <View style={{flexDirection: 'row'}}>
                                <Text style={{color: '#043673', width: 100, fontWeight: 'bold'}}>Tagihan</Text>
                                <Text> : </Text>
                                <Text style={{color: 'red'}}>Rp. {tagihan}</Text>
                            </View>
                            <View style={{paddingTop: 30, width: '80%', borderRadius: 5}}>
                                <View style={{width: '100%', flexDirection: 'row', justifyContent: 'space-around'}}>
                                    <View style={{width: 100}}>
                                        <Button
                                            onPress={() => {
                                                this.setState({modalDataCustomer: false})
                                            }}
                                            color="#043673"
                                            title="OK"
                                        />
                                    </View>

                                </View>
                            </View>
                        </View>
                    )
                } else {
                    customer = (
                        <View style={{justifyContent:'center',  alignItems:'center'}}>
                            <View style={{padding:50, borderWidth:1, borderColor:'#dedede'}}>
                                <Text>No bills for <Text style={{fontWeight: 'bold'}}>{this.state.plat}</Text></Text>
                            </View>
                            <View style={{paddingTop:50, width: 100}}>
                                <Button
                                    onPress={() => {this.setState({modalDataCustomer: false})}}
                                    color="#043673"
                                    title="OK"
                                />
                            </View>
                        </View>
                    )
                }
            }
        } else {
            customer = (
                <View style={{justifyContent:'center',  alignItems:'center'}}>
                    <View style={{padding:50, borderWidth:1, borderColor:'#dedede'}}>
                        <Text>No Parking Data for <Text style={{fontWeight: 'bold'}}>{this.state.plat}</Text></Text>
                    </View>
                    <View style={{paddingTop:50, width: 100}}>
                        <Button
                            onPress={() => {this.setState({modalDataCustomer: false})}}
                            color="#043673"
                            title="OK"
                        />
                    </View>
                </View>
            )
        }

        let name = null
        if(this.state.nama != ''){
            name = (
                <View>
                    <View style={{justifyContent:'center', alignItems:'center', paddingBottom:10}}>
                        <Text><Text style={{fontWeight:'bold'}}>Pemilik Kendaraan:</Text> {this.state.nama}</Text>
                        <Text><Text style={{fontWeight:'bold'}}>Nomor Plat :</Text> {this.state.plat}</Text>
                    </View>
                    <TouchableOpacity onPress={this.onEdit} style={styles.backButton} activeOpacity={0.9}>
                        <Text style={{ color: '#043673' }}>Edit</Text>
                    </TouchableOpacity>
                </View>
            )
        }else{
            name = (
                <TouchableOpacity onPress={this.onSetName} style={styles.backButton} activeOpacity={0.9}>
                    <Text style={{ color: '#043673' }}>Set Name</Text>
                </TouchableOpacity>
            )
        }

        return (
            <View style={styles.mainMenu}>
                <View style={{  alignItems:'center'}}>
                    <Image
                        style={{width: 270, height: 270}}
                        resizeMode="cover"
                        source={require('../../assets/logo.png')}
                    />
                </View>
                {name}
                <View style={{ flexDirection:'row', justifyContent:'space-around', borderWidth: 0, borderColor: 'white', alignItems: 'center' }}>
                    <TouchableOpacity onPress={this.onPress} style={styles.roundIcon} activeOpacity={0.9}>
                        <MaterialIcons
                            name="qrcode"
                            size={80}
                            color="#4c2a23"
                        />
                        <Text style={{ color: '#4c2a23' }}>Show my QR Code</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this.onCheckParking} style={styles.roundIcon} activeOpacity={0.9}>
                        <MaterialIcons
                            name="car"
                            size={80}
                            color="#4c2a23"
                        />
                        <Text style={{ color: '#4c2a23' }}>Check Parking Status</Text>
                    </TouchableOpacity>
                </View>
                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.state.isQRShow}
                    onRequestClose={() => { this.setState({ isQRShow: false }) }}>
                    <View style={{ flex:1, justifyContent:'center', alignItems: 'center', backgroundColor: 'rgb(199,224,253)', width: '100%' }}>
                        <QRCode
                            value={this.state.nama+';'+this.state.plat}
                            size={250}
                            bgColor='#000'
                            fgColor='white' />
                        <View style={{paddingTop:60}}>
                        </View>
                        <TouchableOpacity onPress={this.onFinish} style={styles.backButton} activeOpacity={0.9}>
                            <Text style={{ color: '#043673' }}>Finish</Text>
                        </TouchableOpacity>
                    </View>
                </Modal>
                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.state.isSettingName}
                    onRequestClose={() => { this.setState({ isSettingName: false }) }}>
                    <View style={{ flex:1, justifyContent:'center', alignItems: 'center', backgroundColor: 'white', width: '100%' }}>
                        <View style={{paddingBottom:30}}>
                            <Text style={{fontSize:16, fontWeight:'bold'}}> INFORMASI PEMILIK KENDARAAN </Text>
                        </View>
                        <DefaultInput
                            placeholder="Tulis Nama Anda"
                            value={this.state.nama}
                            onChangeText={val => this.setState({nama: val})}
                            autoCorrect={false}
                            keyboardType="email-address"
                            style={{width:'80%'}}
                        />
                        <DefaultInput
                            placeholder="Nomor Plat Mobil (misal: B 1234 KLQ)"
                            value={this.state.plat}
                            onChangeText={val => this.setState({plat: val})}
                            autoCorrect={false}
                            keyboardType="email-address"
                            style={{width:'80%'}}
                        />
                        <View style={{paddingTop:30}}>
                        </View>
                        <TouchableOpacity onPress={this.onFinishSetName} style={styles.backButton} activeOpacity={0.9}>
                            <Text style={{ color: '#043673' }}>Finish</Text>
                        </TouchableOpacity>
                    </View>
                </Modal>
                <Modal
                    animationType="none"
                    transparent={false}
                    visible={this.state.modalDataCustomer}
                    onRequestClose={() => {
                        this.setState({modalDataCustomer: false})
                    }}>
                    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                        <View style={{paddingBottom: 50}}>
                            <Text style={{fontSize: 20, color: '#043673'}}>I-TIX PARKING BILL</Text>
                        </View>
                        <View style={{justifyContent:'center', alignItems:'center', padding: 10}}>
                            {customer}
                        </View>
                    </View>
                </Modal>
            </View >
        )
    }
}

const styles = StyleSheet.create({
    mainMenu: {
        flex: 1,
        backgroundColor:'white',
        justifyContent:'center',
        alignItems: 'center'
    },
    roundIcon: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 200,
        height: 200,
        backgroundColor: '#fff',
    },
    backButton: {
        paddingHorizontal:30,
        paddingVertical:10,
        alignItems: 'center',
        borderWidth:1,
        justifyContent: 'center',
        backgroundColor: '#fff',
        borderRadius: 20,
    },
})

export default connect(null, null)(MainMenu);